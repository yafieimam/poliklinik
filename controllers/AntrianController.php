<?php

namespace app\controllers;

use Yii;
use app\models\Antrian;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AntrianController implements the CRUD actions for Antrian model.
 */
class AntrianController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Antrian models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Antrian::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Antrian model.
     * @param integer $ID_PASIEN
     * @param integer $ID_ADMIN
     * @param integer $ID_ANTRIAN
     * @return mixed
     */
    public function actionView($ID_PASIEN, $ID_ADMIN, $ID_ANTRIAN)
    {
        return $this->render('view', [
            'model' => $this->findModel($ID_PASIEN, $ID_ADMIN, $ID_ANTRIAN),
        ]);
    }

    /**
     * Creates a new Antrian model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Antrian();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'ID_PASIEN' => $model->ID_PASIEN, 'ID_ADMIN' => $model->ID_ADMIN, 'ID_ANTRIAN' => $model->ID_ANTRIAN]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Antrian model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $ID_PASIEN
     * @param integer $ID_ADMIN
     * @param integer $ID_ANTRIAN
     * @return mixed
     */
    public function actionUpdate($ID_PASIEN, $ID_ADMIN, $ID_ANTRIAN)
    {
        $model = $this->findModel($ID_PASIEN, $ID_ADMIN, $ID_ANTRIAN);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'ID_PASIEN' => $model->ID_PASIEN, 'ID_ADMIN' => $model->ID_ADMIN, 'ID_ANTRIAN' => $model->ID_ANTRIAN]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Antrian model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $ID_PASIEN
     * @param integer $ID_ADMIN
     * @param integer $ID_ANTRIAN
     * @return mixed
     */
    public function actionDelete($ID_PASIEN, $ID_ADMIN, $ID_ANTRIAN)
    {
        $this->findModel($ID_PASIEN, $ID_ADMIN, $ID_ANTRIAN)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Antrian model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $ID_PASIEN
     * @param integer $ID_ADMIN
     * @param integer $ID_ANTRIAN
     * @return Antrian the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($ID_PASIEN, $ID_ADMIN, $ID_ANTRIAN)
    {
        if (($model = Antrian::findOne(['ID_PASIEN' => $ID_PASIEN, 'ID_ADMIN' => $ID_ADMIN, 'ID_ANTRIAN' => $ID_ANTRIAN])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
