<?php

namespace app\controllers;

use Yii;
use app\models\KirimLaborat;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KirimLaboratController implements the CRUD actions for KirimLaborat model.
 */
class KirimLaboratController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KirimLaborat models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => KirimLaborat::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KirimLaborat model.
     * @param integer $ID_DOKTER
     * @param integer $ID_KIRIM
     * @return mixed
     */
    public function actionView($ID_DOKTER, $ID_KIRIM)
    {
        return $this->render('view', [
            'model' => $this->findModel($ID_DOKTER, $ID_KIRIM),
        ]);
    }

    /**
     * Creates a new KirimLaborat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KirimLaborat();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'ID_DOKTER' => $model->ID_DOKTER, 'ID_KIRIM' => $model->ID_KIRIM]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing KirimLaborat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $ID_DOKTER
     * @param integer $ID_KIRIM
     * @return mixed
     */
    public function actionUpdate($ID_DOKTER, $ID_KIRIM)
    {
        $model = $this->findModel($ID_DOKTER, $ID_KIRIM);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'ID_DOKTER' => $model->ID_DOKTER, 'ID_KIRIM' => $model->ID_KIRIM]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing KirimLaborat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $ID_DOKTER
     * @param integer $ID_KIRIM
     * @return mixed
     */
    public function actionDelete($ID_DOKTER, $ID_KIRIM)
    {
        $this->findModel($ID_DOKTER, $ID_KIRIM)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KirimLaborat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $ID_DOKTER
     * @param integer $ID_KIRIM
     * @return KirimLaborat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($ID_DOKTER, $ID_KIRIM)
    {
        if (($model = KirimLaborat::findOne(['ID_DOKTER' => $ID_DOKTER, 'ID_KIRIM' => $ID_KIRIM])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
