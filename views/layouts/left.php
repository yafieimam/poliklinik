<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;

AppAsset::register($this);
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <?php
                if (!Yii::$app->user->isGuest){
                    echo '<div class="pull-left image">';
                    echo '<img src="' . $directoryAsset . '/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>';
                    echo '</div>';
                    echo '<div class="pull-left info">';
                    echo '<p>' . Yii::$app->user->identity->name . '</p>';
                    echo '<a href="#"><i class="fa fa-circle text-success"></i> Online</a>';
                    echo '</div>';
                }
                ?>
            <!-- <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div> -->
        </div>

        <!-- search form -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Home', 'url' => ['/site/index']],                   
                    ['label' => 'Obat', 'url' => ['/obat/index'], 'visible' => !Yii::$app->user->isGuest && (Yii::$app->user->identity->role == User::ROLE_ADMIN || Yii::$app->user->identity->role == User::ROLE_DOKTER)],
                    [
                        'label' => 'Dokter',
                        'items' => [
                            ['label' => 'Data Dokter', 'url' => ['/dokter/index'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_ADMIN],
                            ['label' => 'Jadwal Dokter', 'url' => ['/jadwal/index'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_ADMIN],
                        ],
                        'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_ADMIN,
                    ],
                    ['label' => 'Jadwal Dokter', 'url' => ['/jadwal/index'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_DOKTER],
                    ['label' => 'Antrian', 'url' => ['/antrian/index'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_ADMIN],
                    ['label' => 'Rekam Medis', 'url' => ['/rekam-medis/index'], 'visible' => !Yii::$app->user->isGuest && (Yii::$app->user->identity->role == User::ROLE_DOKTER || Yii::$app->user->identity->role == User::ROLE_PERAWAT)],
                    ['label' => 'Pasien', 'url' => ['/pasien/index'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_DOKTER],
                    ['label' => 'Admin', 'url' => ['/administrator/index'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_ADMIN],
                    ['label' => 'Perawat', 'url' => ['/perawat/index'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_ADMIN],
                    [
                        'label' => 'Laborat',
                        'items' => [
                            ['label' => 'Data Laborat', 'url' => ['/laborat/index'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_DOKTER],
                            ['label' => 'Data Kirim Laborat', 'url' => ['/kirim-laborat/index'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_DOKTER],
                        ],
                        'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_DOKTER,
                    ],
                    ['label' => 'Konsultasi', 'url' => ['/konsultasi-psikologis/index'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_PERAWAT],
                    ['label' => 'Login', 'url' => ['/site/login'], 'visible' => Yii::$app->user->isGuest],

                ],
            ]
        ) ?>

    </section>

</aside>
