<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Dokter */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dokters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create Dokter');
?>
<div class="dokter-create">

    <h1><?= Html::encode(Yii::t('app', 'Create Dokter')) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
