<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' ';
$this->params['breadcrumbs'][] = Yii::t('app', 'Data Dokter');
?>
<div class="dokter-index">

    <h1><?= Html::encode(Yii::t('app', 'Data Dokter')) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Dokter'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'NAMA_DOKTER',
            'ID_DOKTER',
            'ID_JADWAL',
            'KELAMINDOKTER',
            'TGL_LAHIR_DOKTER',
            // 'ALAMAT_DOKTER:ntext',
            // 'TELEPON_DOKTER',
            // 'KEAHLIAN:ntext',
            // 'STATUS:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
