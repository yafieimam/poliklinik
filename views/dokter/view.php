<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Dokter */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dokters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->NAMA_DOKTER;
?>
<div class="dokter-view">

    <h1><?= Html::encode($model->NAMA_DOKTER) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID_DOKTER], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID_DOKTER], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'NAMA_DOKTER',
            'ID_DOKTER',
            'ID_JADWAL',
            'KELAMINDOKTER',
            'TGL_LAHIR_DOKTER',
            'ALAMAT_DOKTER:ntext',
            'TELEPON_DOKTER',
            'KEAHLIAN:ntext',
            'STATUS:ntext',
        ],
    ]) ?>

</div>
