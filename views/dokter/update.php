<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dokter */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dokters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NAMA_DOKTER, 'url' => ['view', 'id' => $model->NAMA_DOKTER]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dokter-update">

    <h1><?= Html::encode(Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Dokter',
]) . $model->NAMA_DOKTER) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
