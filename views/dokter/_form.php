<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dokter */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dokter-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'NAMA_DOKTER')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ID_DOKTER')->textInput() ?>

    <?= $form->field($model, 'ID_JADWAL')->textInput() ?>

    <?= $form->field($model, 'KELAMINDOKTER')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TGL_LAHIR_DOKTER')->textInput() ?>

    <?= $form->field($model, 'ALAMAT_DOKTER')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'TELEPON_DOKTER')->textInput() ?>

    <?= $form->field($model, 'KEAHLIAN')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'STATUS')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
