<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Administrator */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Administrators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create Administrator');
?>
<div class="administrator-create">

    <h1><?= Html::encode(Yii::t('app', 'Create Administrator')) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
