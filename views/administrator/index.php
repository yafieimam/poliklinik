<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' ';
$this->params['breadcrumbs'][] = Yii::t('app', 'Data Administrator');
?>
<div class="administrator-index">

    <h1><?= Html::encode(Yii::t('app', 'Data Administrator')) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Administrator'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'NAMA_ADMIN',
            'ID_ADMIN',
            'TELEPON_ADMIN',
            'ALAMAT_ADMIN:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
