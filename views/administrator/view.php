<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Administrator */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Administrators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->NAMA_ADMIN;
?>
<div class="administrator-view">

    <h1><?= Html::encode($model->NAMA_ADMIN) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID_ADMIN], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID_ADMIN], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'NAMA_ADMIN',
            'ID_ADMIN',
            'TELEPON_ADMIN',
            'ALAMAT_ADMIN:ntext',
        ],
    ]) ?>

</div>
