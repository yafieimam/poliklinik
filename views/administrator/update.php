<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Administrator */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Administrators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_ADMIN, 'url' => ['view', 'id' => $model->NAMA_ADMIN]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="administrator-update">

    <h1><?= Html::encode(Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Administrator',
]) . $model->NAMA_ADMIN) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
