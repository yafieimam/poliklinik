<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Jadwal */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jadwals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create Jadwal');
?>
<div class="jadwal-create">

    <h1><?= Html::encode(Yii::t('app', 'Create Jadwal')) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
