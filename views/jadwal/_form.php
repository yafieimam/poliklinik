<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Jadwal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jadwal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'TIPE_JADWAL')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ID_JADWAL')->textInput() ?>

    <?= $form->field($model, 'PERIODE_JADWAL')->textInput() ?>

    <?= $form->field($model, 'WAKTU_MASUK')->textInput() ?>

    <?= $form->field($model, 'WAKTU_PULANG')->textInput() ?>

    <?= $form->field($model, 'ISTIRAHAT')->textInput() ?>

    <?= $form->field($model, 'KETERANGAN_JADWAL')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'LAMA')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
