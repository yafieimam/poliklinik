<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Jadwal */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Jadwal',
]) . $model->ID_JADWAL;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jadwals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_JADWAL, 'url' => ['view', 'id' => $model->ID_JADWAL]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="jadwal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
