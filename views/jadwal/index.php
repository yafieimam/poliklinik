<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Jadwal Dokter');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jadwal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Jadwal'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'TIPE_JADWAL',
            'ID_JADWAL',
            'PERIODE_JADWAL',
            'WAKTU_MASUK',
            'WAKTU_PULANG',
            // 'ISTIRAHAT',
            // 'KETERANGAN_JADWAL:ntext',
            // 'LAMA',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
