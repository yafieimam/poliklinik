<?php

use yii\helpers\Html;
use app\models\User;

/* @var $this yii\web\View */

$this->title = ' ';
?>
<div class="site-index">

    <div class="jumbotron">
        <?php
        if (!Yii::$app->user->isGuest){
            echo '<h1>Welcome, ' . Yii::$app->user->identity->name . '</h1>';
        ?>
        <p class="lead">Selamat Datang di Website Poliklinik.</p>

        <?php
            if(!Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_ADMIN){
                echo '<div class="row">';
                echo '<div class="dashIcon span2 btn btn-default">';
                echo '<a href="http://localhost/poli/web/obat/index">' .  Html::img('@web/img/icon-picker.png') . '</a>';
                echo '<div class="dashIconText ">';
                echo '<a href="http://localhost/poli/web/obat/index">Data Obat</a>';
                echo '</div>';
                echo '</div>';
                echo '<div class="dashIcon span2 btn btn-default">';
                echo '<a href="http://localhost/poli/web/dokter/index">' .  Html::img('@web/img/icon-bag-medicine.png') . '</a>';
                echo '<div class="dashIconText ">';
                echo '<a href="http://localhost/poli/web/dokter/index">Data Dokter</a>';
                echo '</div>';
                echo '</div>';
                echo '<div class="dashIcon span2 btn btn-default">';
                echo '<a href="http://localhost/poli/web/jadwal/index">' .  Html::img('@web/img/icon-calendar4.png') . '</a>';
                echo '<div class="dashIconText ">';
                echo '<a href="http://localhost/poli/web/jadwal/index">Jadwal Dokter</a>';
                echo '</div>';
                echo '</div>';
                echo '<div class="dashIcon span2 btn btn-default">';
                echo '<a href="http://localhost/poli/web/antrian/index">' .  Html::img('@web/img/icon-people.png') . '</a>';
                echo '<div class="dashIconText ">';
                echo '<a href="http://localhost/poli/web/antrian/index">Data Antrian</a>';
                echo '</div>';
                echo '</div>';
                echo '<div class="dashIcon span2 btn btn-default">';
                echo '<a href="http://localhost/poli/web/administrator/index">' .  Html::img('@web/img/icon-man-tie.png') . '</a>';
                echo '<div class="dashIconText ">';
                echo '<a href="http://localhost/poli/web/administrator/index">Data Admin</a>';
                echo '</div>';
                echo '</div>';
                echo '<div class="dashIcon span2 btn btn-default">';
                echo '<a href="http://localhost/poli/web/perawat/index">' .  Html::img('@web/img/icon-bandaid.png') . '</a>';
                echo '<div class="dashIconText ">';
                echo '<a href="http://localhost/poli/web/perawat/index">Data Perawat</a>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
            }elseif(!Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_DOKTER){
                echo '<div class="row">';
                echo '<div class="dashIcon span2 btn btn-default">';
                echo '<a href="http://localhost/poli/web/obat/index">' .  Html::img('@web/img/icon-picker.png') . '</a>';
                echo '<div class="dashIconText ">';
                echo '<a href="http://localhost/poli/web/obat/index">Data Obat</a>';
                echo '</div>';
                echo '</div>';
                echo '<div class="dashIcon span2 btn btn-default">';
                echo '<a href="http://localhost/poli/web/jadwal/index">' .  Html::img('@web/img/icon-calendar4.png') . '</a>';
                echo '<div class="dashIconText ">';
                echo '<a href="http://localhost/poli/web/jadwal/index">Jadwal Dokter</a>';
                echo '</div>';
                echo '</div>';
                echo '<div class="dashIcon span2 btn btn-default">';
                echo '<a href="http://localhost/poli/web/rekam-medis/index">' .  Html::img('@web/img/icon-book3.png') . '</a>';
                echo '<div class="dashIconText ">';
                echo '<a href="http://localhost/poli/web/rekam-medis/index">Data Rekam Medis</a>';
                echo '</div>';
                echo '</div>';
                echo '<div class="dashIcon span2 btn btn-default">';
                echo '<a href="http://localhost/poli/web/pasien/index">' .  Html::img('@web/img/icon-people4.png') . '</a>';
                echo '<div class="dashIconText ">';
                echo '<a href="http://localhost/poli/web/pasien/index">Data Pasien</a>';
                echo '</div>';
                echo '</div>';
                echo '<div class="dashIcon span2 btn btn-default">';
                echo '<a href="http://localhost/poli/web/laborat/index">' .  Html::img('@web/img/icon-clipboard.png') . '</a>';
                echo '<div class="dashIconText ">';
                echo '<a href="http://localhost/poli/web/laborat/index">Data Laborat</a>';
                echo '</div>';
                echo '</div>';
                echo '<div class="dashIcon span2 btn btn-default">';
                echo '<a href="http://localhost/poli/web/kirim-laborat/index">' .  Html::img('@web/img/icon-download.png') . '</a>';
                echo '<div class="dashIconText ">';
                echo '<a href="http://localhost/poli/web/kirim-laborat/index">Data Kirim Laborat</a>';
                echo '</div>';
                echo '</div>';
            }elseif(!Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_PERAWAT){
                echo '<div class="dashIcon span2 btn btn-default">';
                echo '<a href="http://localhost/poli/web/rekam-medis/index">' .  Html::img('@web/img/icon-book3.png') . '</a>';
                echo '<div class="dashIconText ">';
                echo '<a href="http://localhost/poli/web/rekam-medis/index">Data Rekam Medis</a>';
                echo '</div>';
                echo '</div>';
                echo '<div class="dashIcon span2 btn btn-default">';
                echo '<a href="http://localhost/poli/web/konsultasi-psikologis/index">' .  Html::img('@web/img/icon-people2.png') . '</a>';
                echo '<div class="dashIconText ">';
                echo '<a href="http://localhost/poli/web/konsultasi-psikologis/index">Konsultasi Psikologis</a>';
                echo '</div>';
                echo '</div>';
            }
        }
        ?>
    </div>
</div>
