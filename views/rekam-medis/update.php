<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RekamMedis */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Rekam Medis',
]) . $model->ID_REKAM_MEDIS;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rekam Medis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_REKAM_MEDIS, 'url' => ['view', 'id' => $model->ID_REKAM_MEDIS]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="rekam-medis-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
