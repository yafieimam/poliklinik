<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RekamMedis */

$this->title = $model->ID_REKAM_MEDIS;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rekam Medis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rekam-medis-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID_REKAM_MEDIS], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID_REKAM_MEDIS], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_REKAM_MEDIS',
            'ID_DOKTER',
            'ID_PASIEN',
            'TANGGAL_REKAM_MEDIS',
            'KETERANGAN_REKAM:ntext',
        ],
    ]) ?>

</div>
