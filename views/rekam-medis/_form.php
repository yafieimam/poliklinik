<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RekamMedis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rekam-medis-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID_REKAM_MEDIS')->textInput() ?>

    <?= $form->field($model, 'ID_DOKTER')->textInput() ?>

    <?= $form->field($model, 'ID_PASIEN')->textInput() ?>

    <?= $form->field($model, 'TANGGAL_REKAM_MEDIS')->textInput() ?>

    <?= $form->field($model, 'KETERANGAN_REKAM')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
