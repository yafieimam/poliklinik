<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Antrian */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Antrians'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_PASIEN, 'url' => ['view', 'ID_PASIEN' => $model->ID_PASIEN, 'ID_ADMIN' => $model->ID_ADMIN, 'ID_ANTRIAN' => $model->ID_ANTRIAN]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="antrian-update">

    <h1><?= Html::encode(Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Antrian',
]) . $model->ID_PASIEN) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
