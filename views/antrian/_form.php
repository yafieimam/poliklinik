<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Antrian */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="antrian-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID_PASIEN')->textInput() ?>

    <?= $form->field($model, 'ID_ADMIN')->textInput() ?>

    <?= $form->field($model, 'ID_ANTRIAN')->textInput() ?>

    <?= $form->field($model, 'URUTAN')->textInput() ?>

    <?= $form->field($model, 'TANGGAL_ANTRI')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
