<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Antrian */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Antrians'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->ID_PASIEN;
?>
<div class="antrian-view">

    <h1><?= Html::encode($model->ID_PASIEN) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'ID_PASIEN' => $model->ID_PASIEN, 'ID_ADMIN' => $model->ID_ADMIN, 'ID_ANTRIAN' => $model->ID_ANTRIAN], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'ID_PASIEN' => $model->ID_PASIEN, 'ID_ADMIN' => $model->ID_ADMIN, 'ID_ANTRIAN' => $model->ID_ANTRIAN], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_PASIEN',
            'ID_ADMIN',
            'ID_ANTRIAN',
            'URUTAN',
            'TANGGAL_ANTRI',
        ],
    ]) ?>

</div>
