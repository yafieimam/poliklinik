<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' ';
$this->params['breadcrumbs'][] = Yii::t('app', 'Antrian');
?>
<div class="antrian-index">

    <h1><?= Html::encode(Yii::t('app', 'Antrian')) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Antrian'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID_PASIEN',
            'ID_ADMIN',
            'ID_ANTRIAN',
            'URUTAN',
            'TANGGAL_ANTRI',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
