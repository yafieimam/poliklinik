<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Antrian */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Antrians'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create Antrian');
?>
<div class="antrian-create">

    <h1><?= Html::encode(Yii::t('app', 'Create Antrian')) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
