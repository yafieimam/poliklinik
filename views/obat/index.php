<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' ';
$this->params['breadcrumbs'][] = Yii::t('app', 'Obat');
?>
<div class="obat-index">

    <h1><?= Html::encode(Yii::t('app', 'Obat')) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Obat'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID_OBAT',
            'ID_ADMIN',
            'NAMA_OBAT:ntext',
            'JENIS_OBAT:ntext',
            'STOK',
            // 'KHASIAT:ntext',
            // 'GENERIK',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
