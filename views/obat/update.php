<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Obat */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Obats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_OBAT, 'url' => ['view', 'id' => $model->NAMA_OBAT]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="obat-update">

    <h1><?= Html::encode(Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Obat',
]) . $model->NAMA_OBAT) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
