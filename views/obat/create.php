<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Obat */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Obats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create Obat');
?>
<div class="obat-create">

    <h1><?= Html::encode(Yii::t('app', 'Create Obat')) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
