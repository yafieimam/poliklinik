<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Obat */

$this->title = " ";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Obats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->NAMA_OBAT;
?>
<div class="obat-view">

    <h1><?= Html::encode($model->NAMA_OBAT) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID_OBAT], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID_OBAT], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_OBAT',
            'ID_ADMIN',
            'NAMA_OBAT:ntext',
            'JENIS_OBAT:ntext',
            'STOK',
            'KHASIAT:ntext',
            'GENERIK',
        ],
    ]) ?>

</div>
