<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Obat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="obat-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID_OBAT')->textInput() ?>

    <?= $form->field($model, 'ID_ADMIN')->textInput() ?>

    <?= $form->field($model, 'NAMA_OBAT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'JENIS_OBAT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'STOK')->textInput() ?>

    <?= $form->field($model, 'KHASIAT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'GENERIK')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
