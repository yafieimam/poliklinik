<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pasien */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pasien-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'NAMA_PASIEN')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ID_PASIEN')->textInput() ?>

    <?= $form->field($model, 'ANT_ID_PASIEN')->textInput() ?>

    <?= $form->field($model, 'ID_ADMIN')->textInput() ?>

    <?= $form->field($model, 'ID_ANTRIAN')->textInput() ?>

    <?= $form->field($model, 'JENIS_KELAMIN_PASIEN')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TANGGAL_LAHIR_PASIEN')->textInput() ?>

    <?= $form->field($model, 'ALAMAT_PASIEN')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'RIWAYAT_PENYAKIT_PASIEN')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'KELUHAN')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'STATUS_PASIEN')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
