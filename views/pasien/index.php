<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Data Pasien');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pasien-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Pasien'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'NAMA_PASIEN',
            'ID_PASIEN',
            'ANT_ID_PASIEN',
            'ID_ADMIN',
            'ID_ANTRIAN',
            // 'JENIS_KELAMIN_PASIEN',
            // 'TANGGAL_LAHIR_PASIEN',
            // 'ALAMAT_PASIEN:ntext',
            // 'RIWAYAT_PENYAKIT_PASIEN:ntext',
            // 'KELUHAN:ntext',
            // 'STATUS_PASIEN:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
