<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pasien */

$this->title = $model->ID_PASIEN;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pasiens'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pasien-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID_PASIEN], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID_PASIEN], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'NAMA_PASIEN',
            'ID_PASIEN',
            'ANT_ID_PASIEN',
            'ID_ADMIN',
            'ID_ANTRIAN',
            'JENIS_KELAMIN_PASIEN',
            'TANGGAL_LAHIR_PASIEN',
            'ALAMAT_PASIEN:ntext',
            'RIWAYAT_PENYAKIT_PASIEN:ntext',
            'KELUHAN:ntext',
            'STATUS_PASIEN:ntext',
        ],
    ]) ?>

</div>
