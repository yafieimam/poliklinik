<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KonsultasiPsikologis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="konsultasi-psikologis-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID_KONSUL_PSIKOLOGIS')->textInput() ?>

    <?= $form->field($model, 'ID_PASIEN')->textInput() ?>

    <?= $form->field($model, 'ID_DOKTER')->textInput() ?>

    <?= $form->field($model, 'TANGGAL_KONSUL')->textInput() ?>

    <?= $form->field($model, 'KETERANGAN_KONSUL')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
