<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Konsultasi Psikologis');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="konsultasi-psikologis-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Konsultasi Psikologis'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID_KONSUL_PSIKOLOGIS',
            'ID_PASIEN',
            'ID_DOKTER',
            'TANGGAL_KONSUL',
            'KETERANGAN_KONSUL:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
