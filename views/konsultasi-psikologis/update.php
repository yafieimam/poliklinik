<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KonsultasiPsikologis */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Konsultasi Psikologis',
]) . $model->ID_KONSUL_PSIKOLOGIS;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Konsultasi Psikologis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_KONSUL_PSIKOLOGIS, 'url' => ['view', 'id' => $model->ID_KONSUL_PSIKOLOGIS]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="konsultasi-psikologis-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
