<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KonsultasiPsikologis */

$this->title = Yii::t('app', 'Create Konsultasi Psikologis');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Konsultasi Psikologis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="konsultasi-psikologis-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
