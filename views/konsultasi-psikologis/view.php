<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\KonsultasiPsikologis */

$this->title = $model->ID_KONSUL_PSIKOLOGIS;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Konsultasi Psikologis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="konsultasi-psikologis-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID_KONSUL_PSIKOLOGIS], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID_KONSUL_PSIKOLOGIS], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_KONSUL_PSIKOLOGIS',
            'ID_PASIEN',
            'ID_DOKTER',
            'TANGGAL_KONSUL',
            'KETERANGAN_KONSUL:ntext',
        ],
    ]) ?>

</div>
