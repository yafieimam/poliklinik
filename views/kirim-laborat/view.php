<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\KirimLaborat */

$this->title = $model->ID_DOKTER;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kirim Laborats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kirim-laborat-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'ID_DOKTER' => $model->ID_DOKTER, 'ID_KIRIM' => $model->ID_KIRIM], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'ID_DOKTER' => $model->ID_DOKTER, 'ID_KIRIM' => $model->ID_KIRIM], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_DOKTER',
            'ID_KIRIM',
            'ID_LABORAT',
            'JUDUL_KIRIM',
            'ISI_KIRIM:ntext',
        ],
    ]) ?>

</div>
