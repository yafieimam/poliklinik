<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KirimLaborat */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Kirim Laborat',
]) . $model->ID_DOKTER;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kirim Laborats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_DOKTER, 'url' => ['view', 'ID_DOKTER' => $model->ID_DOKTER, 'ID_KIRIM' => $model->ID_KIRIM]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="kirim-laborat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
