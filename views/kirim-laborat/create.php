<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KirimLaborat */

$this->title = Yii::t('app', 'Create Kirim Laborat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kirim Laborats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kirim-laborat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
