<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Perawat */

$this->title = $model->ID_PERAWAT;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perawats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perawat-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID_PERAWAT], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID_PERAWAT], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'NAMA_PERAWAT',
            'ID_PERAWAT',
            'JENIS_KELAMIN_PERAWAT',
            'TELEPON_PERAWAT',
            'ALAMAT_PERAWAT:ntext',
            'TGL_LAHIR_PERAWAT',
        ],
    ]) ?>

</div>
