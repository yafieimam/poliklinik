<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Data Perawat');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perawat-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Perawat'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'NAMA_PERAWAT',
            'ID_PERAWAT',
            'JENIS_KELAMIN_PERAWAT',
            'TELEPON_PERAWAT',
            'ALAMAT_PERAWAT:ntext',
            // 'TGL_LAHIR_PERAWAT',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
