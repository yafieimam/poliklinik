<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Perawat */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Perawat',
]) . $model->ID_PERAWAT;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perawats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_PERAWAT, 'url' => ['view', 'id' => $model->ID_PERAWAT]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="perawat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
