<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Laborat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="laborat-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID_LABORAT')->textInput() ?>

    <?= $form->field($model, 'ID_DOKTER')->textInput() ?>

    <?= $form->field($model, 'ID_KIRIM')->textInput() ?>

    <?= $form->field($model, 'NAMA_LABORAT')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'KETERANGAN_LABORAT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'LOKASI_LABORAT')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
