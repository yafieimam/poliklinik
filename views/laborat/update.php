<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Laborat */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Laborat',
]) . $model->ID_LABORAT;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laborats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_LABORAT, 'url' => ['view', 'id' => $model->ID_LABORAT]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="laborat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
