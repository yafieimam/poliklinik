<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Laborat */

$this->title = Yii::t('app', 'Create Laborat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laborats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="laborat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
