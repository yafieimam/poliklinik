<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasien".
 *
 * @property string $NAMA_PASIEN
 * @property integer $ID_PASIEN
 * @property integer $ANT_ID_PASIEN
 * @property integer $ID_ADMIN
 * @property integer $ID_ANTRIAN
 * @property string $JENIS_KELAMIN_PASIEN
 * @property string $TANGGAL_LAHIR_PASIEN
 * @property string $ALAMAT_PASIEN
 * @property string $RIWAYAT_PENYAKIT_PASIEN
 * @property string $KELUHAN
 * @property string $STATUS_PASIEN
 *
 * @property Antrian[] $antrians
 * @property KonsultasiPsikologis[] $konsultasiPsikologis
 * @property Antrian $aNTIDPASIEN
 * @property RekamMedis[] $rekamMedis
 */
class Pasien extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pasien';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_PASIEN'], 'required'],
            [['ID_PASIEN', 'ANT_ID_PASIEN', 'ID_ADMIN', 'ID_ANTRIAN'], 'integer'],
            [['TANGGAL_LAHIR_PASIEN'], 'safe'],
            [['ALAMAT_PASIEN', 'RIWAYAT_PENYAKIT_PASIEN', 'KELUHAN', 'STATUS_PASIEN'], 'string'],
            [['NAMA_PASIEN'], 'string', 'max' => 30],
            [['JENIS_KELAMIN_PASIEN'], 'string', 'max' => 2],
            [['ANT_ID_PASIEN', 'ID_ADMIN', 'ID_ANTRIAN'], 'exist', 'skipOnError' => true, 'targetClass' => Antrian::className(), 'targetAttribute' => ['ANT_ID_PASIEN' => 'ID_PASIEN', 'ID_ADMIN' => 'ID_ADMIN', 'ID_ANTRIAN' => 'ID_ANTRIAN']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'NAMA_PASIEN' => 'Nama  Pasien',
            'ID_PASIEN' => 'Id  Pasien',
            'ANT_ID_PASIEN' => 'Ant  Id  Pasien',
            'ID_ADMIN' => 'Id  Admin',
            'ID_ANTRIAN' => 'Id  Antrian',
            'JENIS_KELAMIN_PASIEN' => 'Jenis  Kelamin  Pasien',
            'TANGGAL_LAHIR_PASIEN' => 'Tanggal  Lahir  Pasien',
            'ALAMAT_PASIEN' => 'Alamat  Pasien',
            'RIWAYAT_PENYAKIT_PASIEN' => 'Riwayat  Penyakit  Pasien',
            'KELUHAN' => 'Keluhan',
            'STATUS_PASIEN' => 'Status  Pasien',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAntrians()
    {
        return $this->hasMany(Antrian::className(), ['ID_PASIEN' => 'ID_PASIEN']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKonsultasiPsikologis()
    {
        return $this->hasMany(KonsultasiPsikologis::className(), ['ID_PASIEN' => 'ID_PASIEN']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getANTIDPASIEN()
    {
        return $this->hasOne(Antrian::className(), ['ID_PASIEN' => 'ANT_ID_PASIEN', 'ID_ADMIN' => 'ID_ADMIN', 'ID_ANTRIAN' => 'ID_ANTRIAN']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRekamMedis()
    {
        return $this->hasMany(RekamMedis::className(), ['ID_PASIEN' => 'ID_PASIEN']);
    }
}
