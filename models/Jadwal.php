<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jadwal".
 *
 * @property string $TIPE_JADWAL
 * @property integer $ID_JADWAL
 * @property string $PERIODE_JADWAL
 * @property string $WAKTU_MASUK
 * @property string $WAKTU_PULANG
 * @property string $ISTIRAHAT
 * @property string $KETERANGAN_JADWAL
 * @property integer $LAMA
 *
 * @property Dokter[] $dokters
 */
class Jadwal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jadwal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_JADWAL'], 'required'],
            [['ID_JADWAL', 'LAMA'], 'integer'],
            [['PERIODE_JADWAL', 'WAKTU_MASUK', 'WAKTU_PULANG', 'ISTIRAHAT'], 'safe'],
            [['KETERANGAN_JADWAL'], 'string'],
            [['TIPE_JADWAL'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'TIPE_JADWAL' => 'Tipe  Jadwal',
            'ID_JADWAL' => 'Id  Jadwal',
            'PERIODE_JADWAL' => 'Periode  Jadwal',
            'WAKTU_MASUK' => 'Waktu  Masuk',
            'WAKTU_PULANG' => 'Waktu  Pulang',
            'ISTIRAHAT' => 'Istirahat',
            'KETERANGAN_JADWAL' => 'Keterangan  Jadwal',
            'LAMA' => 'Lama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDokters()
    {
        return $this->hasMany(Dokter::className(), ['ID_JADWAL' => 'ID_JADWAL']);
    }
}
