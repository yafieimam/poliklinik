<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "perawat".
 *
 * @property string $NAMA_PERAWAT
 * @property integer $ID_PERAWAT
 * @property string $JENIS_KELAMIN_PERAWAT
 * @property integer $TELEPON_PERAWAT
 * @property string $ALAMAT_PERAWAT
 * @property string $TGL_LAHIR_PERAWAT
 */
class Perawat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'perawat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_PERAWAT'], 'required'],
            [['ID_PERAWAT', 'TELEPON_PERAWAT'], 'integer'],
            [['ALAMAT_PERAWAT'], 'string'],
            [['TGL_LAHIR_PERAWAT'], 'safe'],
            [['NAMA_PERAWAT'], 'string', 'max' => 30],
            [['JENIS_KELAMIN_PERAWAT'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'NAMA_PERAWAT' => 'Nama  Perawat',
            'ID_PERAWAT' => 'Id  Perawat',
            'JENIS_KELAMIN_PERAWAT' => 'Jenis  Kelamin  Perawat',
            'TELEPON_PERAWAT' => 'Telepon  Perawat',
            'ALAMAT_PERAWAT' => 'Alamat  Perawat',
            'TGL_LAHIR_PERAWAT' => 'Tgl  Lahir  Perawat',
        ];
    }
}
