<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "antrian".
 *
 * @property integer $ID_PASIEN
 * @property integer $ID_ADMIN
 * @property integer $ID_ANTRIAN
 * @property integer $URUTAN
 * @property string $TANGGAL_ANTRI
 *
 * @property Pasien $iDPASIEN
 * @property Administrator $iDADMIN
 * @property Pasien[] $pasiens
 */
class Antrian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'antrian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_PASIEN', 'ID_ADMIN', 'ID_ANTRIAN'], 'required'],
            [['ID_PASIEN', 'ID_ADMIN', 'ID_ANTRIAN', 'URUTAN'], 'integer'],
            [['TANGGAL_ANTRI'], 'safe'],
            [['ID_PASIEN'], 'exist', 'skipOnError' => true, 'targetClass' => Pasien::className(), 'targetAttribute' => ['ID_PASIEN' => 'ID_PASIEN']],
            [['ID_ADMIN'], 'exist', 'skipOnError' => true, 'targetClass' => Administrator::className(), 'targetAttribute' => ['ID_ADMIN' => 'ID_ADMIN']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_PASIEN' => 'Id  Pasien',
            'ID_ADMIN' => 'Id  Admin',
            'ID_ANTRIAN' => 'Id  Antrian',
            'URUTAN' => 'Urutan',
            'TANGGAL_ANTRI' => 'Tanggal  Antri',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDPASIEN()
    {
        return $this->hasOne(Pasien::className(), ['ID_PASIEN' => 'ID_PASIEN']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDADMIN()
    {
        return $this->hasOne(Administrator::className(), ['ID_ADMIN' => 'ID_ADMIN']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPasiens()
    {
        return $this->hasMany(Pasien::className(), ['ANT_ID_PASIEN' => 'ID_PASIEN', 'ID_ADMIN' => 'ID_ADMIN', 'ID_ANTRIAN' => 'ID_ANTRIAN']);
    }
}
