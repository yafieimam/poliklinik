<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dokter".
 *
 * @property string $NAMA_DOKTER
 * @property integer $ID_DOKTER
 * @property integer $ID_JADWAL
 * @property string $KELAMINDOKTER
 * @property string $TGL_LAHIR_DOKTER
 * @property string $ALAMAT_DOKTER
 * @property integer $TELEPON_DOKTER
 * @property string $KEAHLIAN
 * @property string $STATUS
 *
 * @property Jadwal $iDJADWAL
 * @property KirimLaborat[] $kirimLaborats
 * @property KonsultasiPsikologis[] $konsultasiPsikologis
 * @property RekamMedis[] $rekamMedis
 */
class Dokter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dokter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_DOKTER'], 'required'],
            [['ID_DOKTER', 'ID_JADWAL', 'TELEPON_DOKTER'], 'integer'],
            [['TGL_LAHIR_DOKTER'], 'safe'],
            [['ALAMAT_DOKTER', 'KEAHLIAN', 'STATUS'], 'string'],
            [['NAMA_DOKTER'], 'string', 'max' => 30],
            [['KELAMINDOKTER'], 'string', 'max' => 2],
            [['ID_JADWAL'], 'exist', 'skipOnError' => true, 'targetClass' => Jadwal::className(), 'targetAttribute' => ['ID_JADWAL' => 'ID_JADWAL']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'NAMA_DOKTER' => 'Nama  Dokter',
            'ID_DOKTER' => 'Id  Dokter',
            'ID_JADWAL' => 'Id  Jadwal',
            'KELAMINDOKTER' => 'Kelamindokter',
            'TGL_LAHIR_DOKTER' => 'Tgl  Lahir  Dokter',
            'ALAMAT_DOKTER' => 'Alamat  Dokter',
            'TELEPON_DOKTER' => 'Telepon  Dokter',
            'KEAHLIAN' => 'Keahlian',
            'STATUS' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDJADWAL()
    {
        return $this->hasOne(Jadwal::className(), ['ID_JADWAL' => 'ID_JADWAL']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKirimLaborats()
    {
        return $this->hasMany(KirimLaborat::className(), ['ID_DOKTER' => 'ID_DOKTER']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKonsultasiPsikologis()
    {
        return $this->hasMany(KonsultasiPsikologis::className(), ['ID_DOKTER' => 'ID_DOKTER']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRekamMedis()
    {
        return $this->hasMany(RekamMedis::className(), ['ID_DOKTER' => 'ID_DOKTER']);
    }
}
