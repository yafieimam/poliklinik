<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "obat".
 *
 * @property integer $ID_OBAT
 * @property integer $ID_ADMIN
 * @property string $NAMA_OBAT
 * @property string $JENIS_OBAT
 * @property integer $STOK
 * @property string $KHASIAT
 * @property integer $GENERIK
 *
 * @property Administrator $iDADMIN
 */
class Obat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'obat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_OBAT'], 'required'],
            [['ID_OBAT', 'ID_ADMIN', 'STOK', 'GENERIK'], 'integer'],
            [['NAMA_OBAT', 'JENIS_OBAT', 'KHASIAT'], 'string'],
            [['ID_ADMIN'], 'exist', 'skipOnError' => true, 'targetClass' => Administrator::className(), 'targetAttribute' => ['ID_ADMIN' => 'ID_ADMIN']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_OBAT' => 'Id  Obat',
            'ID_ADMIN' => 'Id  Admin',
            'NAMA_OBAT' => 'Nama  Obat',
            'JENIS_OBAT' => 'Jenis  Obat',
            'STOK' => 'Stok',
            'KHASIAT' => 'Khasiat',
            'GENERIK' => 'Generik',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDADMIN()
    {
        return $this->hasOne(Administrator::className(), ['ID_ADMIN' => 'ID_ADMIN']);
    }
}
