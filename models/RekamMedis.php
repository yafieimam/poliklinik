<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rekam_medis".
 *
 * @property integer $ID_REKAM_MEDIS
 * @property integer $ID_DOKTER
 * @property integer $ID_PASIEN
 * @property string $TANGGAL_REKAM_MEDIS
 * @property string $KETERANGAN_REKAM
 *
 * @property Dokter $iDDOKTER
 * @property Pasien $iDPASIEN
 */
class RekamMedis extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rekam_medis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_REKAM_MEDIS', 'ID_DOKTER', 'ID_PASIEN'], 'required'],
            [['ID_REKAM_MEDIS', 'ID_DOKTER', 'ID_PASIEN'], 'integer'],
            [['TANGGAL_REKAM_MEDIS'], 'safe'],
            [['KETERANGAN_REKAM'], 'string'],
            [['ID_DOKTER'], 'exist', 'skipOnError' => true, 'targetClass' => Dokter::className(), 'targetAttribute' => ['ID_DOKTER' => 'ID_DOKTER']],
            [['ID_PASIEN'], 'exist', 'skipOnError' => true, 'targetClass' => Pasien::className(), 'targetAttribute' => ['ID_PASIEN' => 'ID_PASIEN']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_REKAM_MEDIS' => 'Id  Rekam  Medis',
            'ID_DOKTER' => 'Id  Dokter',
            'ID_PASIEN' => 'Id  Pasien',
            'TANGGAL_REKAM_MEDIS' => 'Tanggal  Rekam  Medis',
            'KETERANGAN_REKAM' => 'Keterangan  Rekam',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDDOKTER()
    {
        return $this->hasOne(Dokter::className(), ['ID_DOKTER' => 'ID_DOKTER']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDPASIEN()
    {
        return $this->hasOne(Pasien::className(), ['ID_PASIEN' => 'ID_PASIEN']);
    }
}
