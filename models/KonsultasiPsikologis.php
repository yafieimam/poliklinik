<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "konsultasi_psikologis".
 *
 * @property integer $ID_KONSUL_PSIKOLOGIS
 * @property integer $ID_PASIEN
 * @property integer $ID_DOKTER
 * @property string $TANGGAL_KONSUL
 * @property string $KETERANGAN_KONSUL
 *
 * @property Pasien $iDPASIEN
 * @property Dokter $iDDOKTER
 */
class KonsultasiPsikologis extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'konsultasi_psikologis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_KONSUL_PSIKOLOGIS', 'ID_PASIEN', 'ID_DOKTER'], 'required'],
            [['ID_KONSUL_PSIKOLOGIS', 'ID_PASIEN', 'ID_DOKTER'], 'integer'],
            [['TANGGAL_KONSUL'], 'safe'],
            [['KETERANGAN_KONSUL'], 'string'],
            [['ID_PASIEN'], 'exist', 'skipOnError' => true, 'targetClass' => Pasien::className(), 'targetAttribute' => ['ID_PASIEN' => 'ID_PASIEN']],
            [['ID_DOKTER'], 'exist', 'skipOnError' => true, 'targetClass' => Dokter::className(), 'targetAttribute' => ['ID_DOKTER' => 'ID_DOKTER']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_KONSUL_PSIKOLOGIS' => 'Id  Konsul  Psikologis',
            'ID_PASIEN' => 'Id  Pasien',
            'ID_DOKTER' => 'Id  Dokter',
            'TANGGAL_KONSUL' => 'Tanggal  Konsul',
            'KETERANGAN_KONSUL' => 'Keterangan  Konsul',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDPASIEN()
    {
        return $this->hasOne(Pasien::className(), ['ID_PASIEN' => 'ID_PASIEN']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDDOKTER()
    {
        return $this->hasOne(Dokter::className(), ['ID_DOKTER' => 'ID_DOKTER']);
    }
}
