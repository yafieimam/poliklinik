<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "administrator".
 *
 * @property string $NAMA_ADMIN
 * @property integer $ID_ADMIN
 * @property integer $TELEPON_ADMIN
 * @property string $ALAMAT_ADMIN
 *
 * @property Antrian[] $antrians
 * @property Obat[] $obats
 */
class Administrator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'administrator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_ADMIN'], 'required'],
            [['ID_ADMIN', 'TELEPON_ADMIN'], 'integer'],
            [['ALAMAT_ADMIN'], 'string'],
            [['NAMA_ADMIN'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'NAMA_ADMIN' => 'Nama  Admin',
            'ID_ADMIN' => 'Id  Admin',
            'TELEPON_ADMIN' => 'Telepon  Admin',
            'ALAMAT_ADMIN' => 'Alamat  Admin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAntrians()
    {
        return $this->hasMany(Antrian::className(), ['ID_ADMIN' => 'ID_ADMIN']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObats()
    {
        return $this->hasMany(Obat::className(), ['ID_ADMIN' => 'ID_ADMIN']);
    }
}
