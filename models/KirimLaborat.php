<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kirim_laborat".
 *
 * @property integer $ID_DOKTER
 * @property integer $ID_KIRIM
 * @property integer $ID_LABORAT
 * @property string $JUDUL_KIRIM
 * @property string $ISI_KIRIM
 *
 * @property Dokter $iDDOKTER
 * @property Laborat $iDLABORAT
 * @property Laborat[] $laborats
 */
class KirimLaborat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kirim_laborat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_DOKTER', 'ID_KIRIM', 'ID_LABORAT'], 'required'],
            [['ID_DOKTER', 'ID_KIRIM', 'ID_LABORAT'], 'integer'],
            [['ISI_KIRIM'], 'string'],
            [['JUDUL_KIRIM'], 'string', 'max' => 40],
            [['ID_DOKTER'], 'exist', 'skipOnError' => true, 'targetClass' => Dokter::className(), 'targetAttribute' => ['ID_DOKTER' => 'ID_DOKTER']],
            [['ID_LABORAT'], 'exist', 'skipOnError' => true, 'targetClass' => Laborat::className(), 'targetAttribute' => ['ID_LABORAT' => 'ID_LABORAT']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_DOKTER' => 'Id  Dokter',
            'ID_KIRIM' => 'Id  Kirim',
            'ID_LABORAT' => 'Id  Laborat',
            'JUDUL_KIRIM' => 'Judul  Kirim',
            'ISI_KIRIM' => 'Isi  Kirim',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDDOKTER()
    {
        return $this->hasOne(Dokter::className(), ['ID_DOKTER' => 'ID_DOKTER']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDLABORAT()
    {
        return $this->hasOne(Laborat::className(), ['ID_LABORAT' => 'ID_LABORAT']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLaborats()
    {
        return $this->hasMany(Laborat::className(), ['ID_DOKTER' => 'ID_DOKTER', 'ID_KIRIM' => 'ID_KIRIM']);
    }
}
