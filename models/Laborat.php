<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "laborat".
 *
 * @property integer $ID_LABORAT
 * @property integer $ID_DOKTER
 * @property integer $ID_KIRIM
 * @property string $NAMA_LABORAT
 * @property string $KETERANGAN_LABORAT
 * @property string $LOKASI_LABORAT
 *
 * @property KirimLaborat[] $kirimLaborats
 * @property KirimLaborat $iDDOKTER
 */
class Laborat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laborat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_LABORAT'], 'required'],
            [['ID_LABORAT', 'ID_DOKTER', 'ID_KIRIM'], 'integer'],
            [['KETERANGAN_LABORAT', 'LOKASI_LABORAT'], 'string'],
            [['NAMA_LABORAT'], 'string', 'max' => 30],
            [['ID_DOKTER', 'ID_KIRIM'], 'exist', 'skipOnError' => true, 'targetClass' => KirimLaborat::className(), 'targetAttribute' => ['ID_DOKTER' => 'ID_DOKTER', 'ID_KIRIM' => 'ID_KIRIM']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_LABORAT' => 'Id  Laborat',
            'ID_DOKTER' => 'Id  Dokter',
            'ID_KIRIM' => 'Id  Kirim',
            'NAMA_LABORAT' => 'Nama  Laborat',
            'KETERANGAN_LABORAT' => 'Keterangan  Laborat',
            'LOKASI_LABORAT' => 'Lokasi  Laborat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKirimLaborats()
    {
        return $this->hasMany(KirimLaborat::className(), ['ID_LABORAT' => 'ID_LABORAT']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDDOKTER()
    {
        return $this->hasOne(KirimLaborat::className(), ['ID_DOKTER' => 'ID_DOKTER', 'ID_KIRIM' => 'ID_KIRIM']);
    }
}
