<?php

use yii\db\Migration;

class m161107_020725_create_users extends Migration
{
    public function up()
    {
        $this->createTable('users', [ 
            'id' => $this->primaryKey(), 
            'username' => $this->string()->notNull()->unique(), 
            'authKey' => $this->string(32)->notNull(), 
            'password' => $this->string()->notNull(), 
            'email' => $this->string()->notNull()->unique(), 
            'accessToken' => $this->string(32)->notNull(),
            'role' => $this->string(2)->notNull(), 
        ]);
    }

    public function down()
    {
        $this->dropTable('users');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
