-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2016 at 12:42 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `poliklinik`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `NAMA_ADMIN` varchar(30) DEFAULT NULL,
  `ID_ADMIN` int(11) NOT NULL,
  `TELEPON_ADMIN` int(11) DEFAULT NULL,
  `ALAMAT_ADMIN` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`NAMA_ADMIN`, `ID_ADMIN`, `TELEPON_ADMIN`, `ALAMAT_ADMIN`) VALUES
('Fiqur', 1, 812826181, 'Pamekasan');

-- --------------------------------------------------------

--
-- Table structure for table `antrian`
--

CREATE TABLE `antrian` (
  `ID_PASIEN` int(11) NOT NULL,
  `ID_ADMIN` int(11) NOT NULL,
  `ID_ANTRIAN` int(11) NOT NULL,
  `URUTAN` int(11) DEFAULT NULL,
  `TANGGAL_ANTRI` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `antrian`
--

INSERT INTO `antrian` (`ID_PASIEN`, `ID_ADMIN`, `ID_ANTRIAN`, `URUTAN`, `TANGGAL_ANTRI`) VALUES
(1, 1, 1, 1, '2016-11-17 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `dokter`
--

CREATE TABLE `dokter` (
  `NAMA_DOKTER` varchar(30) DEFAULT NULL,
  `ID_DOKTER` int(11) NOT NULL,
  `ID_JADWAL` int(11) DEFAULT NULL,
  `KELAMINDOKTER` varchar(2) DEFAULT NULL,
  `TGL_LAHIR_DOKTER` datetime DEFAULT NULL,
  `ALAMAT_DOKTER` text,
  `TELEPON_DOKTER` int(11) DEFAULT NULL,
  `KEAHLIAN` text,
  `STATUS` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokter`
--

INSERT INTO `dokter` (`NAMA_DOKTER`, `ID_DOKTER`, `ID_JADWAL`, `KELAMINDOKTER`, `TGL_LAHIR_DOKTER`, `ALAMAT_DOKTER`, `TELEPON_DOKTER`, `KEAHLIAN`, `STATUS`) VALUES
('Yafie', 1, 1, 'LK', '1996-06-27 10:00:00', 'Surabaya', 812986292, 'Penyakit Mata', 'Aktif'),
('Yunaz', 2, 2, 'LK', '1996-10-06 10:00:00', 'Madiun', 815567816, 'Penyakit Kelamin', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `TIPE_JADWAL` varchar(5) DEFAULT NULL,
  `ID_JADWAL` int(11) NOT NULL,
  `PERIODE_JADWAL` datetime DEFAULT NULL,
  `WAKTU_MASUK` time DEFAULT NULL,
  `WAKTU_PULANG` time DEFAULT NULL,
  `ISTIRAHAT` time DEFAULT NULL,
  `KETERANGAN_JADWAL` text,
  `LAMA` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`TIPE_JADWAL`, `ID_JADWAL`, `PERIODE_JADWAL`, `WAKTU_MASUK`, `WAKTU_PULANG`, `ISTIRAHAT`, `KETERANGAN_JADWAL`, `LAMA`) VALUES
('DINAS', 1, '2016-11-15 00:00:00', '08:00:00', '20:00:00', '13:00:00', 'Masuk Pagi', 12),
('DINAS', 2, '2016-11-14 00:00:00', '08:00:00', '20:00:00', '13:00:00', 'Masuk Pagi ', 12);

-- --------------------------------------------------------

--
-- Table structure for table `kirim_laborat`
--

CREATE TABLE `kirim_laborat` (
  `ID_DOKTER` int(11) NOT NULL,
  `ID_KIRIM` int(11) NOT NULL,
  `ID_LABORAT` int(11) NOT NULL,
  `JUDUL_KIRIM` varchar(40) DEFAULT NULL,
  `ISI_KIRIM` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kirim_laborat`
--

INSERT INTO `kirim_laborat` (`ID_DOKTER`, `ID_KIRIM`, `ID_LABORAT`, `JUDUL_KIRIM`, `ISI_KIRIM`) VALUES
(1, 1, 1, 'Hasil Tes Mata', 'Minus 3');

-- --------------------------------------------------------

--
-- Table structure for table `konsultasi_psikologis`
--

CREATE TABLE `konsultasi_psikologis` (
  `ID_KONSUL_PSIKOLOGIS` int(11) NOT NULL,
  `ID_PASIEN` int(11) NOT NULL,
  `ID_DOKTER` int(11) NOT NULL,
  `TANGGAL_KONSUL` datetime DEFAULT NULL,
  `KETERANGAN_KONSUL` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konsultasi_psikologis`
--

INSERT INTO `konsultasi_psikologis` (`ID_KONSUL_PSIKOLOGIS`, `ID_PASIEN`, `ID_DOKTER`, `TANGGAL_KONSUL`, `KETERANGAN_KONSUL`) VALUES
(1, 1, 2, '2016-11-18 00:00:00', 'Psikologinya Gak Jelas'),
(2, 3, 2, '2016-11-07 00:00:00', 'Psikologis terganggu'),
(3, 7, 1, '2016-11-06 00:00:00', 'Gejala psikopat'),
(4, 7, 1, '2016-11-06 00:00:00', 'Gejala psikopat'),
(5, 8, 1, '2016-11-09 00:00:00', 'Gejala psikopat'),
(6, 2, 1, '2016-11-06 00:00:00', 'Psikopat akut');

-- --------------------------------------------------------

--
-- Table structure for table `laborat`
--

CREATE TABLE `laborat` (
  `ID_LABORAT` int(11) NOT NULL,
  `ID_DOKTER` int(11) DEFAULT NULL,
  `ID_KIRIM` int(11) DEFAULT NULL,
  `NAMA_LABORAT` varchar(30) DEFAULT NULL,
  `KETERANGAN_LABORAT` text,
  `LOKASI_LABORAT` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laborat`
--

INSERT INTO `laborat` (`ID_LABORAT`, `ID_DOKTER`, `ID_KIRIM`, `NAMA_LABORAT`, `KETERANGAN_LABORAT`, `LOKASI_LABORAT`) VALUES
(1, 1, 1, 'Laborat Mata', 'Menyimpan hasil tes laboratorium mata', 'Lantai 2'),
(2, 1, 1, 'Laboratorium Check Darah', 'Digunakan untuk pengecekan darah', 'Lantai 2'),
(3, 1, 1, 'Laboratorium Kelamin', 'Digunakan untuk pengecekan penyakit kelamin', 'Lantai 3'),
(4, 1, 1, 'Laboratorium Fisum', 'Digunakan untuk pengecekan fisum', 'Lantai 3'),
(5, 1, 1, 'Laboratorium Penyakit dalam', 'Digunakan untuk pengecekan penyakit dalam', 'Lantai 4'),
(6, 1, 1, 'Laboratorium Penyakit Luar', 'Digunakan untuk pengecekan penyakit luar', 'Lantai 5');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `options` text COLLATE utf8_unicode_ci,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1478258343),
('m150312_121633_Menu_Items', 1478258348),
('m161107_020725_create_users', 1478484723);

-- --------------------------------------------------------

--
-- Table structure for table `obat`
--

CREATE TABLE `obat` (
  `ID_OBAT` int(11) NOT NULL,
  `ID_ADMIN` int(11) DEFAULT NULL,
  `NAMA_OBAT` text,
  `JENIS_OBAT` text,
  `STOK` int(11) DEFAULT NULL,
  `KHASIAT` text,
  `GENERIK` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obat`
--

INSERT INTO `obat` (`ID_OBAT`, `ID_ADMIN`, `NAMA_OBAT`, `JENIS_OBAT`, `STOK`, `KHASIAT`, `GENERIK`) VALUES
(1, 1, 'Paramex', 'Tablet', 180, 'menyembuhkan pusing kepala', 1),
(2, 1, 'Konimex', 'Tablet', 169, 'Meredakan batuk', 1),
(3, 1, 'paracetamol', 'ringan - penurun panas', 10, 'menurunkan panas', 1),
(4, 1, 'amoxilin', 'ringan - badan', 200, 'antibiotik badans', 1),
(5, 1, 'promaag', 'ringan - maag', 250, 'mencegah gejala maag', 1),
(6, 1, 'bodrex', 'ringan - penurun panas', 100, 'menurunkan panas', 1),
(7, 1, 'mastin', 'ringan - suplemen', 10, 'suplemen tubuh', 1),
(8, 1, 'oskadon', 'ringan - pusing', 105, 'meredakan pusing', 1),
(9, 1, 'bodrex - flu', 'ringan - penurun pilek', 10, 'meredakan pilek', 1),
(11, 1, 'Paramex', 'Pil', 10, 'penyakit', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pasien`
--

CREATE TABLE `pasien` (
  `NAMA_PASIEN` varchar(30) DEFAULT NULL,
  `ID_PASIEN` int(11) NOT NULL,
  `ANT_ID_PASIEN` int(11) DEFAULT NULL,
  `ID_ADMIN` int(11) DEFAULT NULL,
  `ID_ANTRIAN` int(11) DEFAULT NULL,
  `JENIS_KELAMIN_PASIEN` varchar(2) DEFAULT NULL,
  `TANGGAL_LAHIR_PASIEN` datetime DEFAULT NULL,
  `ALAMAT_PASIEN` text,
  `RIWAYAT_PENYAKIT_PASIEN` text,
  `KELUHAN` text,
  `STATUS_PASIEN` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pasien`
--

INSERT INTO `pasien` (`NAMA_PASIEN`, `ID_PASIEN`, `ANT_ID_PASIEN`, `ID_ADMIN`, `ID_ANTRIAN`, `JENIS_KELAMIN_PASIEN`, `TANGGAL_LAHIR_PASIEN`, `ALAMAT_PASIEN`, `RIWAYAT_PENYAKIT_PASIEN`, `KELUHAN`, `STATUS_PASIEN`) VALUES
('Imam', 1, 1, 1, 1, 'L', '1996-07-19 00:00:00', 'Surabaya', 'Sakit Mata', 'Mata Merah', 'Rawat Inap'),
('yunaz', 2, 1, 1, 1, 'L', '1996-01-23 00:00:00', 'Jl. Wiraraja no 7 Winongo Madiun', 'Pernah mengalami gejala maag', 'Perut sakit', 'aktif'),
('taufiqur', 3, 1, 1, 1, 'L', '1995-01-23 00:00:00', 'Jl. Mulyosari no 8 Sukolilo Surabaya', '-', 'Sakit pusing', 'aktif'),
('dimas', 4, 1, 1, 1, 'L', '1996-10-03 00:00:00', 'Jl. Semampir no 90 Sidoarjo', 'Mata minus', 'cek mata', 'aktif'),
('aditya', 5, 1, 1, 1, 'L', '1996-05-30 00:00:00', 'Jl. Ngawur no 0 Ponorogo', 'Sakit Rambut', 'Rambut bergelombang', 'aktif'),
('tasia', 6, 1, 1, 1, 'P', '1996-01-20 00:00:00', 'Jl. Ngawi no 1 Ngawi KOTA', 'Pernah mengalami penyakit sinus', 'Sakit influenza', 'aktif'),
('febri', 7, 1, 1, 1, 'P', '1995-01-23 00:00:00', 'Jl. Ketuban no 100 Tuban', 'Memiliki lemak jahat', 'Lemak mengamuk', 'aktif'),
('hasna', 8, 1, 1, 1, 'P', '1995-06-23 00:00:00', 'Jl. Sudirman no 9 Jakarta Pusat', 'Pernah mengalami gejala maag', 'Sakit panas', 'aktif');

-- --------------------------------------------------------

--
-- Table structure for table `perawat`
--

CREATE TABLE `perawat` (
  `NAMA_PERAWAT` varchar(30) DEFAULT NULL,
  `ID_PERAWAT` int(11) NOT NULL,
  `JENIS_KELAMIN_PERAWAT` varchar(2) DEFAULT NULL,
  `TELEPON_PERAWAT` int(11) DEFAULT NULL,
  `ALAMAT_PERAWAT` text,
  `TGL_LAHIR_PERAWAT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perawat`
--

INSERT INTO `perawat` (`NAMA_PERAWAT`, `ID_PERAWAT`, `JENIS_KELAMIN_PERAWAT`, `TELEPON_PERAWAT`, `ALAMAT_PERAWAT`, `TGL_LAHIR_PERAWAT`) VALUES
('Taufiqur', 1, 'LK', 817826171, 'Madura', '1995-11-15 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rekam_medis`
--

CREATE TABLE `rekam_medis` (
  `ID_REKAM_MEDIS` int(11) NOT NULL,
  `ID_DOKTER` int(11) NOT NULL,
  `ID_PASIEN` int(11) NOT NULL,
  `TANGGAL_REKAM_MEDIS` datetime DEFAULT NULL,
  `KETERANGAN_REKAM` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rekam_medis`
--

INSERT INTO `rekam_medis` (`ID_REKAM_MEDIS`, `ID_DOKTER`, `ID_PASIEN`, `TANGGAL_REKAM_MEDIS`, `KETERANGAN_REKAM`) VALUES
(1, 1, 1, '2016-11-17 00:00:00', 'Keluhan Mata Minus'),
(2, 1, 1, '2016-11-08 00:00:00', 'Gejala maag'),
(3, 1, 1, '2016-11-08 00:00:00', 'Gejala maag'),
(4, 2, 1, '2016-11-09 00:00:00', 'Mutaber'),
(5, 2, 1, '2016-11-07 00:00:00', 'Jantung lemah'),
(6, 1, 1, '2016-11-06 00:00:00', 'Pusing migrain'),
(7, 1, 1, '2016-11-10 00:00:00', 'Gejala maag');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `authKey` varchar(32) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `accessToken` varchar(32) NOT NULL,
  `role` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `authKey`, `password`, `name`, `email`, `accessToken`, `role`) VALUES
(1, 'admin', '', '21232f297a57a5a743894a0e4a801fc3', 'Yafie', 'imamachmad18@gmail.com', '', '30'),
(2, 'perawat', '', '5d6a514ee02a5fc910dee69cc07017cc', 'Yunaz', 'yunaz.gilang@gmail.com', '', '20'),
(3, 'dokter', '', 'd22af4180eee4bd95072eb90f94930e5', 'Fiqur', 'fiqur.fr@gmail.com', '', '10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`ID_ADMIN`);

--
-- Indexes for table `antrian`
--
ALTER TABLE `antrian`
  ADD PRIMARY KEY (`ID_PASIEN`,`ID_ADMIN`,`ID_ANTRIAN`),
  ADD KEY `FK_RELATIONSHIP_6` (`ID_ADMIN`);

--
-- Indexes for table `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`ID_DOKTER`),
  ADD KEY `FK_RELATIONSHIP_9` (`ID_JADWAL`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`ID_JADWAL`);

--
-- Indexes for table `kirim_laborat`
--
ALTER TABLE `kirim_laborat`
  ADD PRIMARY KEY (`ID_DOKTER`,`ID_KIRIM`),
  ADD KEY `FK_RELATIONSHIP_13` (`ID_LABORAT`);

--
-- Indexes for table `konsultasi_psikologis`
--
ALTER TABLE `konsultasi_psikologis`
  ADD PRIMARY KEY (`ID_KONSUL_PSIKOLOGIS`),
  ADD KEY `FK_RELATIONSHIP_10` (`ID_PASIEN`),
  ADD KEY `FK_RELATIONSHIP_11` (`ID_DOKTER`);

--
-- Indexes for table `laborat`
--
ALTER TABLE `laborat`
  ADD PRIMARY KEY (`ID_LABORAT`),
  ADD KEY `FK_RELATIONSHIP_14` (`ID_DOKTER`,`ID_KIRIM`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menuItems_name` (`name`),
  ADD KEY `menuItems_visible` (`visible`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `obat`
--
ALTER TABLE `obat`
  ADD PRIMARY KEY (`ID_OBAT`),
  ADD KEY `FK_RELATIONSHIP_15` (`ID_ADMIN`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`ID_PASIEN`),
  ADD KEY `FK_RELATIONSHIP_4` (`ANT_ID_PASIEN`,`ID_ADMIN`,`ID_ANTRIAN`);

--
-- Indexes for table `perawat`
--
ALTER TABLE `perawat`
  ADD PRIMARY KEY (`ID_PERAWAT`);

--
-- Indexes for table `rekam_medis`
--
ALTER TABLE `rekam_medis`
  ADD PRIMARY KEY (`ID_REKAM_MEDIS`),
  ADD KEY `FK_RELATIONSHIP_7` (`ID_DOKTER`),
  ADD KEY `FK_RELATIONSHIP_8` (`ID_PASIEN`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `antrian`
--
ALTER TABLE `antrian`
  ADD CONSTRAINT `FK_RELATIONSHIP_5` FOREIGN KEY (`ID_PASIEN`) REFERENCES `pasien` (`ID_PASIEN`),
  ADD CONSTRAINT `FK_RELATIONSHIP_6` FOREIGN KEY (`ID_ADMIN`) REFERENCES `administrator` (`ID_ADMIN`);

--
-- Constraints for table `dokter`
--
ALTER TABLE `dokter`
  ADD CONSTRAINT `FK_RELATIONSHIP_9` FOREIGN KEY (`ID_JADWAL`) REFERENCES `jadwal` (`ID_JADWAL`);

--
-- Constraints for table `kirim_laborat`
--
ALTER TABLE `kirim_laborat`
  ADD CONSTRAINT `FK_RELATIONSHIP_12` FOREIGN KEY (`ID_DOKTER`) REFERENCES `dokter` (`ID_DOKTER`),
  ADD CONSTRAINT `FK_RELATIONSHIP_13` FOREIGN KEY (`ID_LABORAT`) REFERENCES `laborat` (`ID_LABORAT`);

--
-- Constraints for table `konsultasi_psikologis`
--
ALTER TABLE `konsultasi_psikologis`
  ADD CONSTRAINT `FK_RELATIONSHIP_10` FOREIGN KEY (`ID_PASIEN`) REFERENCES `pasien` (`ID_PASIEN`),
  ADD CONSTRAINT `FK_RELATIONSHIP_11` FOREIGN KEY (`ID_DOKTER`) REFERENCES `dokter` (`ID_DOKTER`);

--
-- Constraints for table `laborat`
--
ALTER TABLE `laborat`
  ADD CONSTRAINT `FK_RELATIONSHIP_14` FOREIGN KEY (`ID_DOKTER`,`ID_KIRIM`) REFERENCES `kirim_laborat` (`ID_DOKTER`, `ID_KIRIM`);

--
-- Constraints for table `obat`
--
ALTER TABLE `obat`
  ADD CONSTRAINT `FK_RELATIONSHIP_15` FOREIGN KEY (`ID_ADMIN`) REFERENCES `administrator` (`ID_ADMIN`);

--
-- Constraints for table `pasien`
--
ALTER TABLE `pasien`
  ADD CONSTRAINT `FK_RELATIONSHIP_4` FOREIGN KEY (`ANT_ID_PASIEN`,`ID_ADMIN`,`ID_ANTRIAN`) REFERENCES `antrian` (`ID_PASIEN`, `ID_ADMIN`, `ID_ANTRIAN`);

--
-- Constraints for table `rekam_medis`
--
ALTER TABLE `rekam_medis`
  ADD CONSTRAINT `FK_RELATIONSHIP_7` FOREIGN KEY (`ID_DOKTER`) REFERENCES `dokter` (`ID_DOKTER`),
  ADD CONSTRAINT `FK_RELATIONSHIP_8` FOREIGN KEY (`ID_PASIEN`) REFERENCES `pasien` (`ID_PASIEN`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
